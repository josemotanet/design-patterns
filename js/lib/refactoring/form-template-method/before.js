class Ticket {
  constructor() {}

  price() {
    return 2.0;
  }
}

class SeniorTicket {
  constructor() {}

  price() {
    return 1.5;
  }
}

class JuniorTicket {
  constructor() {}

  price() {
    return 1.0;
  }
}

module.exports = {
  ticket: Ticket,
  seniorTicket: SeniorTicket,
  juniorTicket: JuniorTicket
};
