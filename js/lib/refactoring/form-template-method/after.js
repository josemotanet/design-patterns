class Ticket {
  constructor() {
    this.basePrice = 2.0;
  }

  discount() {
    return 1.0;
  }

  price() {
    return this.basePrice * this.discount();
  }
}

class SeniorTicket extends Ticket {
  discount() {
    return 0.75;
  }
}

class JuniorTicket extends Ticket {
  discount() {
    return 0.5;
  }
}

module.exports = {
  ticket: Ticket,
  seniorTicket: SeniorTicket,
  juniorTicket: JuniorTicket
};
