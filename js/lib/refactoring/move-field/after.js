'use strict';

class Person {
  constructor(locale, phone) {
    this.phone = new Phone(phone, locale);
  }

  full_phone() {
    return [ '+', this.phone ].join('');
  }
}

class Phone {
  constructor(number, locale) {
    this.number = number;
    this.locale = locale;
  }

  toString() {
    return PHONE_CODES[this.locale] + ' ' + this.number;
  }
}

const PHONE_CODES = {
  en_gb: '44',
  pt:    '351'
};

module.exports = Person;
