'use strict';

class Person {
  constructor(locale, phone) {
    this.locale = locale;
    this.phone = new Phone(phone);
  }

  full_phone() {
    return [ '+', PHONE_CODES[this.locale], ' ', this.phone ].join('');
  }
}

class Phone {
  constructor(number) {
    this.number = number;
  }

  toString() {
    return this.number;
  }
}

const PHONE_CODES = {
  en_gb: '44',
  pt:    '351'
};

module.exports = Person;
