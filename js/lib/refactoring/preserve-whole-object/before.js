'use strict';

class Store {
  constructor() {
    this.items = [];
  }

  add(item) {
    this.items = this.items.concat(item);
  }

  contains(name, price, date) {
    return this.items.find(i => i.name == name && i.price == price && i.date == date);
  }
}

class Item {
  constructor(name, price, date) {
    this.name = name;
    this.price = price;
    this.date = date;
  }
}

module.exports = {
  store: Store,
  item: Item
};
