'use strict';

class Store {
  constructor() {
    this.items = [];
  }

  add(item) {
    this.items = this.items.concat(item);
  }

  contains(item) {
    return this.items.find(i => i.name == item.name &&
                                i.price == item.price &&
                                i.date == item.date);
  }
}

class Item {
  constructor(name, price, date) {
    this.name = name;
    this.price = price;
    this.date = date;
  }
}

module.exports = {
  store: Store,
  item: Item
};
