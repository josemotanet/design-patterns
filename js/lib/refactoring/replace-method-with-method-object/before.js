'use strict';

class TaxSimulator {
  constructor(person) {
    this.person = person;
  }

  simulateReturn(options) {
    var income = options.income || 0;
    var expenses = options.expenses || 0;
    var type = options.type || 'dependentWorker';

    var returnValue = 0;
    var numberOfPeopleUnderRoof = 1;

    if (type == 'dependentWorker') {
      returnValue += income * 0.02;
    } else {
      returnValue += income * 0.04;
    }

    if (numberOfPeopleUnderRoof > 2) {
      returnValue *= 1.10;
    }

    if (income - expenses > income * 0.05) {
      returnValue += expenses * 0.05;
    }

    returnValue -= expenses * 0.30;

    return returnValue;
  }
}

module.exports = TaxSimulator;
