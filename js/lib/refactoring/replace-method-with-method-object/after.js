'use strict';

class TaxSimulator {
  constructor(person) {
    this.person = person;
  }

  simulateReturn(options) {
    var income = options.income || 0;
    var expenses = options.expenses || 0;
    var type = options.type || 'dependentWorker';

    return new TaxAlgorithm(options).compute();

  }
}

class TaxAlgorithm {
  constructor(options) {
    this.income = options.income;
    this.expenses = options.expenses;
    this.type = options.type;

    this.returnValue = 0;
    this.numberOfPeopleUnderRoof = 1;
  }

  compute() {
    this.processType();
    this.processNumberOfPeople();
    this.processIncomeExpenseDifference();
    this.deductExpenses();

    return this.returnValue;
  }

  processType() {
    if (this.type == 'dependentWorker') {
      this.returnValue += this.income * 0.02;
    } else {
      this.returnValue += this.income * 0.04;
    }
  }

  processNumberOfPeople() {
    if (this.numberOfPeopleUnderRoof > 2) {
      this.returnValue *= 1.10;
    }
  }

  processIncomeExpenseDifference() {
    if (this.income - this.expenses > this.income * 0.05) {
      this.returnValue += this.expenses * 0.05;
    }
  }

  deductExpenses() {
    this.returnValue -= this.expenses * 0.30;
  }
}

module.exports = TaxSimulator;
