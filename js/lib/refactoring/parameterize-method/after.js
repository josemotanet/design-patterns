'use strict';

class Student {
  constructor() {
    this.grades = {
      first: 10,
      second: 11,
      third: 12
    };
  }

  term_grade(term) {
    return this.grades[term];
  }
}

module.exports = Student;
