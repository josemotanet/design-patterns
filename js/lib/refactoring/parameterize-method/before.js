'use strict';

class Student {
  first_term_grade() {
    return 10;
  }

  second_term_grade() {
    return 11;
  }

  third_term_grade() {
    return 12;
  }
}

module.exports = Student;
