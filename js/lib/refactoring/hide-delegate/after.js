'use strict';

class Clerk {
  constructor(manager) {
    this.manager = manager;
  }

  getManager() {
    return this.manager;
  }
}

class Manager {}

class Department {
  constructor(manager) {
    this.manager = manager;
  }

  getManager() {
    return this.manager;
  }
}

class Client {
  constructor(clerk) {
    this.clerk = clerk;
  }

  getDepartment() {
    return this.department;
  }

  getClerk() {
    return this.clerk;
  }
}

module.exports = {
  clerk: Clerk,
  manager: Manager,
  department: Department,
  client: Client
};
