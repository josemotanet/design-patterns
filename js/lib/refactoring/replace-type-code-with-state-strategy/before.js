'use strict';

class User {
  constructor(name, type, options) {
    this.name = name;
    this.type = type;
    this.options = options;
  }

  publicKeyMatches() {
    // Compute logic
    return true;
  }

  oauthAuthenticates() {
    // Compute logic
    return true;
  }

  static login(name, options = {}) {
    var user = USERS.find(user => user.name == name);

    switch(user.type) {
    case 'password':
      return user.options.password == options.password;
    case 'publicKey':
      return user.publicKeyMatches();
    case 'oauth':
      return user.oauthAuthenticates();
    }
  }
}

const USERS = [
  new User('Regular user', 'password', { password: 'secret' }),
  new User('Public key user', 'publicKey', { key_location: 'assets/id_rsa.pub' }),
  new User('OAuth user', 'oauth', { provider: 'twitter' })
];

module.exports = User;
