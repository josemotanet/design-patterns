'use strict';

class User {
  constructor(name, type, options) {
    this.name = name;
    this.type = type;
    this.options = options;

    switch(this.type) {
    case 'password':
      this.strategy = new Auth.Password(this);
      break;
    case 'publicKey':
      this.strategy = new Auth.PublicKey(this);
      break;
    case 'oauth':
      this.strategy = new Auth.OAuth(this);
    }
  }

  static login(name) {
    var user = USERS.find(user => user.name == name);
    return user.strategy.auth(user.options);
  }
}

var Auth = {
  Password: function(user) {
    this.auth = function(options = {}) {
      return user.options.password == options.password;
    };
  },

  PublicKey: function(user) {
    this.auth = function(options = {}) {
      // Compute logic
      return true;
    };
  },

  OAuth: function(user) {
    this.auth = function(options = {}) {
      // Compute logic
      return true;
    };
  }
};

const USERS = [
  new User('Regular user', 'password', { password: 'secret' }),
  new User('Public key user', 'publicKey', { key_location: 'assets/id_rsa.pub' }),
  new User('OAuth user', 'oauth', { provider: 'twitter' })
];

module.exports = User;
