"use strict";

var strftime = require('strftime');

class Post {
  constructor(title, date) {
    this.title = title;
    this.date = date;
  }

  body() {
    return `RANDOM TEXT Ladyship it daughter securing procured or am moreover
mr. Put sir she exercise vicinity cheerful wondered. Continual say suspicion
provision you neglected sir curiosity unwilling.`;
  }

  condensedFormat() {
    return `Title: ${this.title}
    Date: ${strftime("%Y/%m/%d", this.date)}`;
  }

  fullFormat() {
    return `Title: ${this.title}
    Date: ${strftime("%Y/%m/%d", this.date)}
    --
    ${this.body()}`;
  }
}

module.exports = Post;
