"use strict";

var strftime = require('strftime');

class Post {
  constructor(title, date) {
    this.title = title;
    this.date = date;
  }

  body() {
    return `RANDOM TEXT Ladyship it daughter securing procured or am moreover
mr. Put sir she exercise vicinity cheerful wondered. Continual say suspicion
provision you neglected sir curiosity unwilling.`;
  }

  metadata() {
    return `Title: ${this.title}
    Date: ${strftime("%Y/%m/%d", this.date)}`;
  }

  condensedFormat() {
    return this.metadata();
  }

  fullFormat() {
    return `${this.metadata()}
    --
    ${this.body()}`;
  }
}

module.exports = Post;
