'use strict';

class Post {
  constructor(id, title, body, created_at) {
    this.id         = id;
    this.title      = title;
    this.body       = body;
    this.created_at = created_at;
    this.published  = false;
  }

  static find_and_publish(id) {
    var post = POSTS.find(p => p.id == id);
    if (typeof post !== 'undefined') {
      post.publish(); return post;
    }
    return new NullPost();
  }

  publish() {
    this.published = true;
  }

  isPublished() {
    return this.published;
  }
}

class NullPost {
  isPublished() { return this; }
}

const POSTS = [
  new Post(
    1,
    "Introduce Null Object Pattern",
    "Post body should be here",
    new Date(2013,0,25)
  )
];

module.exports = Post;
