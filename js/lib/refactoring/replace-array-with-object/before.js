'use strict';

class Cart {
  constructor(products) {
    this.products = products;
  }

  total() {
    return this.products.reduce(function(sum, product) {
      return sum + product[2];
    }, 0);
  }
}

module.exports = Cart;
