'use strict';

class Cart {
  constructor(products) {
    this.products = products.map(product => new Product(...product));
  }

  total() {
    return this.products.reduce((sum, product) => sum + product.price, 0);
  }

}

class Product {
  constructor(name, color, price) {
    this.name  = name;
    this.color = color;
    this.price = price;
  }
}

module.exports = Cart;
