'use strict';

class Post {
  constructor(id, title, body, createdAt, published = false) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.createdAt = createdAt;
    this.published = published;
  }

  unpublish() {
    this.published = false;
  }

  publish() {
    this.published = true;
    return global.POSTS.filter(post => !post.published);
  }

  static find(id) {
    return global.POSTS.find(post => post.id == id);
  }
}

module.exports = Post;
