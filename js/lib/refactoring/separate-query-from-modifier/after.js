'use strict';

class Post {
  constructor(id, title, body, createdAt, published = false) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.createdAt = createdAt;
    this.published = published;
  }

  unpublish() {
    this.published = false;
  }

  publish() {
    this.published = true;
    return this.published;
  }

  static find(id) {
    return global.POSTS.find(post => post.id == id);
  }

  static unpublished() {
    return global.POSTS.filter(post => !post.published);
  }
}

module.exports = Post;
