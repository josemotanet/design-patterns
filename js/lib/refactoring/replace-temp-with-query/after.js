'use strict';

class Cuboid {
  constructor(length, width, height) {
    this.length = length;
    this.width = width;
    this.height = height;
  }

  volume() {
    return this.area() * this.height;
  }

  area() {
    return this.length * this.width;
  }
}

module.exports = Cuboid;
