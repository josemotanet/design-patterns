'use strict';

class Cuboid {
  constructor(length, width, height) {
    this.length = length;
    this.width = width;
    this.height = height;
  }

  volume() {
    var area = this.length * this.width;
    return area * this.height;
  }
}

module.exports = Cuboid;
