'use strict';

class SquareRootCalculator {
  static calculate(i) {
    if (i < 1) {
      return null;
    }

    return Math.sqrt(i);
  }
}

module.exports = SquareRootCalculator;
