'use strict';

class SquareRootCalculator {
  static calculate(i) {
    if (i < 1) {
      throw new TypeError();
    }

    return Math.sqrt(i);
  }
}

module.exports = SquareRootCalculator;
