'use strict';

class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
}

class MalePerson extends Person {
  fullName() {
    return this.firstName + ' ' + this.lastName;
  }

  gender() {
    return 'M';
  }
}

class FemalePerson extends Person {
  fullName() {
    return this.firstName + ' ' + this.lastName;
  }

  gender() {
    return 'F';
  }
}

module.exports = {
  malePerson: MalePerson,
  femalePerson: FemalePerson
};
