'use strict';

class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  fullName() {
    return this.firstName + ' ' + this.lastName;
  }

}

class MalePerson extends Person {
  gender() {
    return 'M';
  }
}

class FemalePerson extends Person {
  gender() {
    return 'F';
  }
}

module.exports = {
  malePerson: MalePerson,
  femalePerson: FemalePerson
};
