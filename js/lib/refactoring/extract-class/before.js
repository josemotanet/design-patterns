"use strict";

var _ = require('lodash');

class Student {
  constructor() {
    this.firstTermAssiduity = 0;
    this.firstTermTest = 0;
    this.firstTermBehavior = 0;

    this.secondTermAssiduity = 0;
    this.secondTermTest = 0;
    this.secondTermBehavior = 0;

    this.thirdTermAssiduity = 0;
    this.thirdTermTest = 0;
    this.thirdTermBehavior = 0;
  }

  setAllGradesTo(grade) {
    var _this = this;
    var terms = ['first', 'second', 'third'];
    var criteria = ['assiduity ', 'test', 'behavior'];

    terms.forEach(function(term) {
      criteria.forEach(function(criterium) {
        var property = _.camelCase(`${term}_term_${criterium}`);
        _this[property] = grade;
      });
    });
  }

  firstTermGrade() {
    return (this.firstTermAssiduity +
            this.firstTermTest +
            this.firstTermBehavior) / 3;
  }

  secondTermGrade() {
    return (this.secondTermAssiduity +
            this.secondTermTest +
            this.secondTermBehavior) / 3;
  }

  thirdTermGrade() {
    return (this.thirdTermAssiduity +
            this.thirdTermTest +
            this.thirdTermBehavior) / 3;
  }
}

module.exports = Student;
