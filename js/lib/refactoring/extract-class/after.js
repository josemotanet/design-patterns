'use strict';

class Student {
  constructor() {
    this.terms = [
      new Term('first'),
      new Term('second'),
      new Term('third')
    ];
  }

  setAllGradesTo(grade) {
    this.terms.forEach(function(term) {
      term.setAllGradesTo(grade);
    });
  }

  firstTermGrade() {
    return this.term('first').grade();
  }

  secondTermGrade() {
    return this.term('second').grade();
  }

  thirdTermGrade() {
    return this.term('third').grade();
  }

  term(reference) {
    return this.terms.find(function(term) {
      return term.name == reference;
    });
  }
}

class Term {
  constructor(name) {
    this.name      = name;
    this.assiduity = 0;
    this.test      = 0;
    this.behavior  = 0;
  }

  setAllGradesTo(grade) {
    this.assiduity = grade;
    this.test      = grade;
    this.behavior  = grade;
  }

  grade() {
    return (this.assiduity + this.test + this.behavior) / 3;
  }
}

module.exports = Student;
