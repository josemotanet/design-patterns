'use strict';

var _ = require('lodash');

class Employee {
  constructor(type = 'regular') {
    this.type = type;
  }

  baseSalary() {
    return 500.0;
  }

  salary() {
    return this.baseSalary() + this.bonus();
  }

  static build(type = 'regular') {
    var instance = new Employee();
    EmployeeModules[_.capitalize(type)].call(instance);
    return instance;
  }
}

var EmployeeModules = {
  Regular: function() {
    this.bonus = function() {
      return 0;
    };
  },

  Manager: function() {
    this.bonus = function() {
      return 800;
    };
  },

  Boss: function() {
    this.bonus = function() {
      return 1500;
    };
  }
};

module.exports = Employee;
