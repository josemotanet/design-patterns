'use strict';

class Employee {
  constructor(type = 'regular') {
    this.type = type;
  }

  base_salary() {
    return 500.0;
  }

  salary() {
    return this.base_salary() + this.bonus();
  }

  bonus() {
    var value = 0;

    switch(this.type) {
    case 'regular':
      value = 0;
      break;
    case 'boss':
      value = 1500.0;
      break;
    case 'manager':
      value = 800.0;
      break;
    }

    return value;
  }

  static build(type = 'regular') {
    return new Employee(type);
  }
}

module.exports = Employee;
