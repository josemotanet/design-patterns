'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var lib = require('../../lib/refactoring/preserve-whole-object/' + (env.BEFORE ? 'before' : 'after'));
var Store = lib.store;
var Item = lib.item;

suite('Store', function() {
  setup(function() {
    this.item = new Item('Macbook Pro', 1299.0, new Date());
  });

  if (env.BEFORE) {
    test('finds an item in the store', function() {
      var subject = new Store();
      subject.add(this.item);
      assert.isOk(subject.contains(this.item.name, this.item.price, this.item.date));
    });
  } else {
    test('finds an item in the store', function() {
      var subject = new Store();
      subject.add(this.item);
      assert.isOk(subject.contains(this.item));
    });
  }
});
