'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Post = require('../../lib/refactoring/separate-query-from-modifier/' + (env.BEFORE ? 'before' : 'after'));

global.POSTS = [
  new Post(
    1,
    "Introduce Null Object Pattern",
    "Post body should be here",
    new Date(2013,0,25)
  ),
  new Post(
    2,
    "Introduce Assertion",
    "Post body should be here",
    new Date(2012,1,26)
  ),
  new Post(
    3,
    "Extract Method",
    "Post body should be here",
    new Date(2014,2,27)
  ),
  new Post(
    4,
    "Replace Type Code with Polymorphism",
    "Post body should be here",
    new Date(2015,9,12)
  )
];

suite('Post', function() {
  setup(function() {
    global.POSTS.forEach(post => post.unpublish());
  });

  if (env.BEFORE) {
    test('is publishable and retrieves all unpublished posts count', function() {
      var post = Post.find(1);
      assert.equal(post.publish().length, 3);
    });
  } else {
    test('is publishable', function() {
      var post = Post.find(1);
      assert.isTrue(post.publish());
    });

    test('retrieves all unpublished posts count', function() {
      assert.equal(Post.unpublished().length, 4);
    });
  }
});
