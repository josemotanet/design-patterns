'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Person = require('../../lib/refactoring/move-field/' + (env.BEFORE ? 'before' : 'after'));

suite('Person', function() {
  test('has a phone number', function() {
    var person = new Person('pt', '555-0342');
    assert.equal(person.full_phone(), '+351 555-0342');
  });
});
