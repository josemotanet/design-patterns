'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Post = require('../../lib/refactoring/extract-method/' + (env.BEFORE ? 'before' : 'after'));

suite('Post', function() {
  setup(function() {
    this.date = new Date(2014, 1, 28);
    this.post = new Post("Fragmented Class", this.date);
  });

  suite('when requested a condensed format', function() {
    test("shows the post's title" , function() {
      assert.include(this.post.condensedFormat(), "Fragmented Class");
    });

    test("shows the post's date" , function() {
      assert.include(this.post.condensedFormat(), "2014/02/28");
    });
  });

  suite("when requested a full format" , function() {
    test("shows the post's title" , function() {
      assert.include(this.post.fullFormat(), "Fragmented Class");
    });

    test("shows the post's date" , function() {
      assert.include(this.post.fullFormat(), "2014/02/28");
    });
  });
});
