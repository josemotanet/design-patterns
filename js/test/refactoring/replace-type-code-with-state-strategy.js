'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var User = require('../../lib/refactoring/replace-type-code-with-state-strategy/' + (env.BEFORE ? 'before' : 'after'));

suite('User', function() {
  test('can log in by default via user/pass combo', function() {
    assert.isOk(User.login('Regular user', { password: 'secret' }));
  });

  test('can log in via a public key', function() {
    assert.isOk(User.login('Public key user'));
  });

  test('can log in via oauth', function() {
    assert.isOk(User.login('OAuth user'));
  });
});
