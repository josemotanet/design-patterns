'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var SquareRootCalculator = require('../../lib/refactoring/introduce-assertion/' + (env.BEFORE ? 'before' : 'after'));

suite("SquareRootCalculator", function() {
  test("calculates a square root", function() {
    assert.equal(3, SquareRootCalculator.calculate(9));
  });

  if (env.BEFORE) {
    test("doesn't calculate a negative number's square root", function() {
      assert.isNull(SquareRootCalculator.calculate(-9));
    });
  } else {
    test("doesn't calculate a negative number's square root", function() {
      assert.throws(function() { SquareRootCalculator.calculate(-9); });
    });
  }
});
