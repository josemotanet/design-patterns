'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var tickets = require('../../lib/refactoring/form-template-method/' + (env.BEFORE ? 'before' : 'after'));
var Ticket = tickets.ticket;
var SeniorTicket = tickets.seniorTicket;
var JuniorTicket = tickets.juniorTicket;

suite('Ticket', function() {
  test('has a calculated price', function() {
    assert.equal(2.0, new Ticket().price());
  });

  test('costs less if a senior', function() {
    assert.equal(new SeniorTicket().price(), 1.5);
  });

  test('costs less if a junior', function() {
    assert.equal(new JuniorTicket().price(), 1.0);
  });
});
