'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Cart = require('../../lib/refactoring/replace-array-with-object/' + (env.BEFORE ? 'before' : 'after'));

suite('Cart', function() {
  test("has a list of items", function() {
    var cart = new Cart([
      [ 'Sweater'   , 'Pink' , 5.0  ],
      [ 'Trousers'  , 'Blue' , 8.0  ],
      [ 'Golf Club' , 'Gray' , 12.0 ]
    ]);

    assert.equal(cart.total(), 25.0);
  });
});
