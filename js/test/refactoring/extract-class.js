'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Student = require('../../lib/refactoring/extract-class/' + (env.BEFORE ? 'before' : 'after'));

suite('Student', function() {
  test("has a grade for all three terms", function() {
    var student = new Student();
    student.setAllGradesTo(10);

    assert.equal(student.firstTermGrade(), 10);
    assert.equal(student.secondTermGrade(), 10);
    assert.equal(student.thirdTermGrade(), 10);
  });
});
