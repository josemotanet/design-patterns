'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Employee = require('../../lib/refactoring/replace-type-code-with-module-extension/' + (env.BEFORE ? 'before' : 'after'));

suite('Employee', function() {
  test("a regular employee has a salary", function() {
    assert.equal(Employee.build().salary(), 500.0);
  });

  test("a boss has a salary", function() {
    assert.equal(Employee.build('boss').salary(), 2000.0);
  });

  test("a manager has a salary", function() {
    assert.equal(Employee.build('manager').salary(), 1300.0);
  });
});
