'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Post = require('../../lib/refactoring/introduce-null-object/' + (env.BEFORE ? 'before' : 'after'));

suite("Post", function() {
  test("is publishable", function() {
    assert.isTrue(Post.find_and_publish(1).isPublished());
  });

  test("does nothing if post is not found", function() {
    Post.find_and_publish(0); // assert nothing raised
  });
});
