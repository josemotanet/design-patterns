'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var lib = require('../../lib/refactoring/pull-up-method/' + (env.BEFORE ? 'before' : 'after'));
var MalePerson = lib.malePerson;
var FemalePerson = lib.femalePerson;

suite('MalePerson', function() {
  test('has a full name', function() {
    assert.equal(new MalePerson('John', 'Smith').fullName(), 'John Smith');
  });
});

suite('FemalePerson', function() {
  test('has a full name', function() {
    assert.equal(new FemalePerson('Michelle', 'Smith').fullName(), 'Michelle Smith');
  });
});
