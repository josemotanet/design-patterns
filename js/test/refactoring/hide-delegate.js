'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var classes = require('../../lib/refactoring/hide-delegate/' + (env.BEFORE ? 'before' : 'after'));
var Clerk = classes.clerk;
var Department = classes.department;
var Manager = classes.manager;
var Client = classes.client;

suite('Client', function() {
  setup(function() {
    this.manager = new Manager();
    this.department = new Department(this.manager);
    this.clerk = new Clerk(this.department);
  });

  if(env.BEFORE) {
    test('wants to know about the manager through a department', function() {
      var client = new Client(this.department, this.clerk);
      assert.isNotNull(client.getDepartment().getManager());
    });
  } else {
    test('should rather be guided by a clerk to access that info', function() {
      var client = new Client(this.clerk);
      assert.isNotNull(client.getClerk().getManager());
    });
  }
});
