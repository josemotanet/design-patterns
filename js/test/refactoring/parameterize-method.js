'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Student = require('../../lib/refactoring/parameterize-method/' + (env.BEFORE ? 'before' : 'after'));

suite('Student', function() {
  if (env.BEFORE) {
    test("has a first grade", function() {
      assert.equal(new Student().first_term_grade(), 10);
    });

    test("has a second grade", function() {
      assert.equal(new Student().second_term_grade(), 11);
    });

    test("has a third grade", function() {
      assert.equal(new Student().third_term_grade(), 12);
    });
  } else {
    test("has a first grade", function() {
      assert.equal(new Student().term_grade("first"), 10);
    });

    test("has a second grade", function() {
      assert.equal(new Student().term_grade("second"), 11);
    });

    test("has a third grade", function() {
      assert.equal(new Student().term_grade("third"), 12);
    });
  }
});
