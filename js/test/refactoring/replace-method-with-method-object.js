'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var TaxSimulator = require('../../lib/refactoring/replace-method-with-method-object/' + (env.BEFORE ? 'before' : 'after'));

suite('TaxSimulator', function() {
  test("simulates tax returns", function() {
    assert.equal(new TaxSimulator("Jose Mota").simulateReturn({
      income: 10000,
      expenses: 300,
      type: 'independent_worker'
    }), 325.0);
  });
});
