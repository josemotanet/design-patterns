'use strict';

var assert = require('chai').assert;
var env = require('process').env;
var Cuboid = require('../../lib/refactoring/replace-temp-with-query/' + (env.BEFORE ? 'before' : 'after'));

suite('Cuboid', function() {
  test("has a volume", function() {
    assert.equal(new Cuboid(2,3,4).volume(), 24);
  });
});
