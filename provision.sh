NODE_VERSION=v6.2.2
RUBY_VERSION=ruby-2.3.1
DEBIAN_FRONTEND=noninteractive

function reset {
    rm -rf $HOME/{.nvm,.rubies,src}
}

function install_libs {
    sudo apt-get -y install linux-headers-$(uname -r) build-essential dkms
}

function install_node {
    echo "Installing node.js..."

    # Get nvm to install one
    wget -qO- --no-check-certificate https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash

    # Edit .bashrc to load nvm
    mv $HOME/bashrc $HOME/.bashrc
    source $HOME/.bashrc

    # Install a Node version
    nvm install $NODE_VERSION >/dev/null 2>&1
    nvm alias default $NODE_VERSION
    echo "Done."
}

function install_ruby {
    echo 'Installing Ruby...'

    ## ruby-install
    wget -q -O ruby-install-0.6.0.tar.gz https://github.com/postmodern/ruby-install/archive/v0.6.0.tar.gz
    tar -xzvf ruby-install-0.6.0.tar.gz
    cd ruby-install-0.6.0/
    sudo make install

    ## chruby
    wget -q -O chruby-0.3.9.tar.gz https://github.com/postmodern/chruby/archive/v0.3.9.tar.gz
    tar -xzvf chruby-0.3.9.tar.gz
    cd chruby-0.3.9/
    sudo make install

    ruby-install $RUBY_VERSION
    echo $RUBY_VERSION > $HOME/.ruby-version

    echo 'Done.'
}

function bootstrap {
    cd $HOME/app; bash bin/bootstrap
    cd
}

reset
install_libs
install_node
install_ruby
bootstrap
