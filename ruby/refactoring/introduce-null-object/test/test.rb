require 'minitest/autorun'
require 'minitest/spec'

require 'before' if ENV['BEFORE']
require 'after' unless ENV['BEFORE']

describe Post do
  it 'is publishable' do
    assert Post.find_and_publish(1).published?
  end

  it 'does nothing if post is not found' do
    Post.find_and_publish(0) # assert nothing raises
  end
end
