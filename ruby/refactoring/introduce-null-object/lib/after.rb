class Post
  attr_reader :id, :title, :body, :created_at, :published
  def initialize(id, title, body, created_at)
    @id         = id
    @title      = title
    @body       = body
    @created_at = created_at
    @published = false
  end

  def self.find_and_publish(id)
    # database operation to retrieve data. We'll simulate it for now.
    post = POSTS.find { |p| p.id == id } || NullPost.new
    post.publish
    post
  end

  def publish
    @published = true
  end

  def published?
    published
  end

end

class NullPost
  def method_missing(method, *args, &block)
    if Post.instance_methods(false).include?(method)
      self
    else
      super
    end
  end
end

POSTS = [
  Post.new(
    1,
    'Introduce Null Object Pattern',
    'Post body should be here',
    Time.new(2013,01,25)
  )
]
