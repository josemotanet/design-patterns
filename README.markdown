# Software Design Patterns, by José Mota

This is the source code for the training course.

## Installation

You can choose to run the examples from your own machine, without
virtualization. They are tested against \*NIX systems like Linux or Mac OS X.
Use Windows at your own risk.

Git is assumed to exist in your machine. If you don't have it, check the [Git
downloads page](https://git-scm.com/downloads) for your system.

You should have one of the following two language environments:

1. Ruby (minimum version: 2.3.0).
2. Javascript on Node (minimum version: 6.2.0).

### Installing with Vagrant

Specifically for Linux, ensure that you also have rsync installed.

* Debian/Ubuntu: `$ sudo apt-get install rsync`
* Fedora/Red Hat: `$ sudo dnf install rsync`

To install the development environment:

* Install [VirtualBox](http://virtualbox.org) and [Vagrant](http://vagrantup.com).
* Open your terminal.
* Clone the repository and enter it with `git clone git://gitlab.com/josemotanet/design-patterns`.
* Boot the VM with `vagrant up --provision`. This will take time.
* Enter the machine with SSH with `vagrant ssh`.
* Navigate to `/home/vagrant/app` to access the training content.

## Usage (Ruby)

For each design pattern under `ruby/`, there is a Rakefile and a test suite. To
run it, type `rake test` in the command line.

## Usage (Javascript)

Go to the `js` folder and type `npm link mocha` to have mocha installed
properly.

To run the test suite for all design patterns, run `npm test`. To run the tests
for a specific pattern, run `npm test test/<path/to/design/pattern>`.
