export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

source /usr/local/share/chruby/chruby.sh
source /usr/local/share/chruby/auto.sh